# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# Cat.create!([ {age: 50, birth_date: Date.new(1963,11,13), color: "orange",
#                name: "Garfield", sex: "M"},
#               {age: 13, birth_date: Date.new(2000,9,13), color: "black",
#                name: "Sennacy", sex: "F"},
#                {age: 18, birth_date: Date.new(1995,9,13), color: "white",
#                 name: "Mufasa", sex: "M"}
#             ])

# CatRentalRequest.create!([
  # {cat_id: 1, start_date: Date.today, end_date: (Date.today + 5), status: "APPROVED"},
  # {cat_id: 2, start_date: Date.today, end_date: (Date.today + 5), status: "APPROVED"},
  # {cat_id: 1, start_date: Date.today, end_date: (Date.today + 10), status: "DENIED"}
# ])

Cat.create!({age: 50, birth_date: Date.new(1963,11,13), color: "orange",
               name: "Bob", sex: "M", user_id: 1})



