class CatRentalRequestsController < ApplicationController

  before_filter :rental_require_current_user!, :except => [:create, :new]

  def new
    @all_cats = Cat.all
    render :new
  end

  def create
    new_request = CatRentalRequest.new(params[:cat_rental_request])
    if new_request.save
      redirect_to cat_url(new_request.cat_id)
    else
      render :json => new_request.errors, :status => :unprocessable_entity
    end
  end

  def approve
    rental_request = CatRentalRequest.find(params[:id])
    rental_request.approve!
    redirect_to rental_request.cat
  end

  def deny
    rental_request = CatRentalRequest.find(params[:id])
    rental_request.deny!
    redirect_to rental_request.cat
  end

end
