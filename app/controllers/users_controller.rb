class UsersController < ApplicationController

  def new
    render :new
  end

  def create
    new_user = User.new(params[:user])
    if new_user.save
      login_user!
    else
      render text: new_user.errors
    end

  end

end
