class SessionsController < ApplicationController

  def new
    render :new
  end

  def create

    login_user!
  end

  def destroy
    logout_current_user!
    redirect_to new_sessions_url
  end

end
