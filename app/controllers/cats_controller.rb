class CatsController < ApplicationController
  before_filter :cat_require_current_user!, :only => [:edit, :update, :destroy]

  def index
    @cats = Cat.all
    render :index
  end

  def show
    @cat = Cat.find(params[:id])
    render :show
  end

  def new
    @cat = Cat.new()
    render :new
  end

  def create
    new_cat = current_user.cats.new(params[:cat])
    if new_cat.save
      redirect_to new_cat
    else
      render :json => new_cat.errors, :status => :unprocessable_entity
    end
  end

  def edit
    @cat = Cat.find(params[:id])
    render :edit
  end

  def update
    Cat.update(params[:id],params[:cat])
    redirect_to Cat.find(params[:id])
  end

  def destroy
    Cat.destroy(params[:id])
    redirect_to cats_url
  end




end
