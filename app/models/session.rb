class Session < ActiveRecord::Base
  attr_accessible :token, :user_id

  validates :user_id, :token, presence: true

  def self.create_session(user)
    Session.create!(user_id: user.id, token: Session.generate_new_token!)
  end

  def self.generate_new_token!
    SecureRandom::urlsafe_base64(16)
  end

  belongs_to(
  :user,
  class_name: "User",
  foreign_key: :user_id,
  primary_key: :id
  )

end
