class CatRentalRequest < ActiveRecord::Base
  attr_accessible :cat_id, :end_date, :start_date, :status

  before_validation(on: :create) { self.status ||= "PENDING" }

  validates :cat_id, :end_date, :start_date, :status, presence: true
  validates :status, inclusion: { in: %w(PENDING APPROVED DENIED),
    message: "%{value} is not a valid status" }
  validate :overlapping_approved_requests

  belongs_to(
    :cat,
    dependent: :destroy,
    class_name: "Cat",
    primary_key: :id,
    foreign_key: :cat_id
  )

  def overlapping_requests
    new_start = self.start_date
    new_end   = self.end_date

    CatRentalRequest.where("id != ? AND cat_id = ? AND NOT (start_date >= ? OR end_date <= ?)", self.id, self.cat_id, new_end, new_start)

  end

  def overlapping_approved_requests
    return if self.status != "APPROVED"
    unless self.overlapping_requests.where(:status == "APPROVED").empty?
      errors[:base] << "Overlapping Approved Requests"
    end
  end

  def approve!
    self.status = "APPROVED"
    if self.save
      self.overlapping_pending_requests.each do |request|
        request.deny!
      end
    end
  end

  def overlapping_pending_requests
    self.overlapping_requests.where(:status == "PENDING")
  end

  def deny!
    self.status = "DENIED"
    self.save!
  end

  def pending?
    self.status == "PENDING"
  end


end
