require 'bcrypt'

class User < ActiveRecord::Base
  attr_accessible :user_name, :password
  attr_reader :password

  validates :password_digest, presence: { :message => "Password can't be blank" }
  validates :user_name, presence: true
  # validates :session_token, presence: true
  validates :user_name, uniqueness: true

  # before_validation(on: :create) { self.session_token ||= reset_session_token! }

  has_many(
    :cats,
    class_name: "Cat",
    foreign_key: :user_id,
    primary_key: :id
  )

  has_many(
  :sessions,
  class_name: "Session",
  foreign_key: :user_id,
  primary_key: :id
  )

  def self.find_by_credentials(user_name, password)
    possible_user = User.find_by_user_name(user_name)
    if possible_user.is_password?(password)
      return possible_user
    end
    nil
  end

  # def reset_session_token!
#     self.session_token = SecureRandom::urlsafe_base64(16)
#   end

  def password=(password)
    @password = password
    unless password.strip == ""
      self.password_digest = BCrypt::Password.create(password)
    end
  end

  def is_password?(password)
    BCrypt::Password.new(self.password_digest).is_password?(password)
  end

end
