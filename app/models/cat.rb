class Cat < ActiveRecord::Base
  COLORS = %w(black brown white orange)
  SEX = ['','M', 'F']
  attr_accessible :age, :birth_date, :color, :name, :sex, :user_id

  validates :age, :birth_date, :color, :name, :sex, :user_id, presence: true
  validates :age, numericality: true
  validates :color, inclusion: { in: %w(black brown white orange),
    message: "%{value} is not a valid color" }
  validates :sex, inclusion: { in: %w(M F),
    message: "%{value} is not a valid sex" }


  has_many(
    :cat_rental_requests,
    class_name: "CatRentalRequest",
    primary_key: :id,
    foreign_key: :cat_id
  )

  belongs_to(
    :user,
    class_name: "User",
    foreign_key: :user_id,
    primary_key: :id
  )

end

