module SessionsHelper

  def current_user=(user)
    @current_user = user
    new_session = Session.create_session(user)
    session[:session_token] = new_session.token
  end

  def current_user
    secret_token = session[:session_token]
    return nil if secret_token.nil?
    session = Session.find_by_token(secret_token)
    if session
      @current_user ||= session.user
    end
  end

  def logout_current_user!
    current_session = Session.find_by_token(session[:session_token])
    Session.destroy(current_session.id)

    # current_user.reset_session_token!
#     current_user.save
#     session[:session_token] = nil #delete row form session table instead
  end

  def login_user!
    user =  User.find_by_credentials(get_user_name, get_password)
    unless user.nil?
      self.current_user = user
      redirect_to cats_url
    else
      render :json => "Credentials were wrong"
    end
  end

  def get_password
    params[:user][:password]
  end

  def get_user_name
    params[:user][:user_name]
  end

  def cat_require_current_user!
    owner_id = Cat.find(params[:id]).user_id
    require_current_user(owner_id)
  end

  def rental_require_current_user!
    owner_id = CatRentalRequest.find(params[:id]).cat.user_id
    require_current_user(owner_id)
  end

  private

    def require_current_user(owner_id)
      redirect_to new_sessions_url unless current_user.id == owner_id
    end

end
